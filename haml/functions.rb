def render(partial, locals = {})
    if(File.exist?(partial.to_s)) then

        locals = locals || {}
        locals["inner"] = true


        Haml::Engine.new( File.read(partial.to_s)).render(Object.new, locals )
    else
        puts "file #{partial.to_s} not exists "
    end
end

def jsondata(path2file)
    if(File.exist?(path2file.to_s)) then
        require "json"
            file = File.read(path2file.to_s)
            return JSON.parse(file)
    else
        return []
    end
end

def fh(path2file)
require 'digest'
    if(File.exist?(path2file)) then
        md5 = Digest::MD5.file(path2file).hexdigest.to_s[-6..-1]
        return md5
     else
        return ''
    end
end